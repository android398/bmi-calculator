package com.natcha.bmicalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.natcha.bmicalculator.databinding.ActivityMainBinding
import java.text.NumberFormat
import kotlin.math.sqrt

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityMainBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.calculateButton.setOnClickListener { calculateBMI() }
    }

    private fun displayBMI(bmi: Double, result: String) {
        val formattedBMI = NumberFormat.getNumberInstance().format(bmi)
        binding.bmiResult.text = getString(R.string.bmi_amount, formattedBMI)
        binding.result.text = result
    }

    private fun calculateBMI() {
        val stringInWeightTextField = binding.weightEditText.text.toString()
        val weight = stringInWeightTextField.toDoubleOrNull()
        val stringInHeightTextField = binding.heightEditText.text.toString()
        val height = stringInHeightTextField.toDoubleOrNull()
        if(weight == null || weight == 0.0 || height == null || height == 0.0) {
            displayBMI(0.0, "")
            return
        }
        var bmi = weight / ((height/100) * (height/100))
        var result = if (bmi < 18.5){
            "UNDERWEIGHT"
        }else if(bmi < 25){
            "NORMAL"
        }else if(bmi < 30){
            "OVERWEIGHT"
        }else{
            "OBESE"
        }
        displayBMI(bmi, result)
    }
}